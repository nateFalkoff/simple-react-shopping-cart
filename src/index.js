import React from 'react';
import ReactDOM from 'react-dom';
import './App.css';
import * as serviceWorker from './serviceWorker';
import blart from './blart.jpg'

class App extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
        count: 0,
        price: 9.99,
        total: 0,
    }

    this.countUp = this.countUp.bind(this)


  }

  countUp() {
    this.setState({ count: this.state.count + 1})
    this.setState({total: this.state.count * this.state.price})
  }


  render() {

    return (
      <div className="App">
        <div className="card">
          <img src={blart} className="scaleimg"></img>
          <div className="container">
            <p>
              {this.state.price}
          </p>
          <button onClick={this.countUp}>
              Add to cart
          </button>
            <p>
              Copies: {this.state.count}
            </p>
            <p>
              Total : {this.state.total.toFixed(2)}
            </p>
          </div>
        </div>
      </div>
    );
  }
}


ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
